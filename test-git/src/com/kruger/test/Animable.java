/**
 * 
 */
package com.kruger.test;

/**
 * @author gcrespo
 *
 */
public interface Animable {
	public static final int animo = 100;
	
	public void animar();
	public void continuar();
}
